<?xml version="1.0"?>
<anjuta>
    <plugin name="Terminal" mandatory="no">
        <require group="Anjuta Plugin"
                 attribute="Location"
                 value="anjuta-terminal:TerminalPlugin"/>
    </plugin>
    <plugin name="Git" mandatory="no">
        <require group="Anjuta Plugin"
                 attribute="Location"
                 value="anjuta-git:Git"/>
    </plugin>
    <plugin name="Tools" mandatory="no">
        <require group="Anjuta Plugin"
                 attribute="Location"
                 value="anjuta-tools:ATPPlugin"/>
    </plugin>
    <plugin name="JHBuild" mandatory="no">
        <require group="Anjuta Plugin"
                 attribute="Location"
                 value="anjuta-jhbuild:JHBuildPlugin"/>
    </plugin>
</anjuta>
